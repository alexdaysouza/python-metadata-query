import os
import pytest
import sqlite3
from metadataquery.app import app, db

# Use a test database URI to avoid altering the production database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://instance//v_product.db'
valid_start_time = '2005-10-13 16:00:00.000'
valid_end_time = '2005-10-13 16:00:00.000'
invalid_start_time = '10-13 16:00:00.000'
invalid_end_time = '10-13 16:00:00.000'
invalid_start_time_response = 'Invalid datetime'
invalid_end_time_response = 'Invalid datetime'

@pytest.fixture
def client():
    with app.test_client() as client:
        with app.app_context():
            db.create_all()
        yield client

def test_table_results(client):
    with app.app_context():
        response = client.get('/')
        assert response.status_code == 200
        assert b"PROD_ID" in response.data
       # assert b"JNLITM_ID:" in response.data
        

def test_filter_by_start_time(client):
    with app.app_context():

       response = client.get(f'/?start-time={invalid_start_time}')
       assert response.status_code == 200
       assert bytes(invalid_start_time_response, 'utf-8') in response.data

      

def test_filter_by_end_time(client):
    with app.app_context():

        response = client.get('/?end-time=2005-10-13 16:00:19.000')
        assert response.status_code == 200
        assert b"2005-10-13 16:00:19.000" in response.data

# Add more test functions for other filtering scenarios

if __name__ == '__main__':
    pytest.main()
